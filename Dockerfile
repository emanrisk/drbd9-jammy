# Pull in DRBD sources and entrypoint
FROM quay.io/piraeusdatastore/drbd9-focal:v9.1.7 as SOURCE

FROM ubuntu:22.04

# Packages needed for the DRBD build process.
RUN apt update && \
    DEBIAN_FRONTEND=noninteractive apt install -y -o DPkg::options::='--force-confdef' -o DPkg::options::='--force-confold' gcc make coccinelle cpio patch perl diffutils kmod elfutils && \
    apt clean

# The DRBD sources and build script from an existing injector image
COPY --from=SOURCE /entry.sh /drbd.tar.gz /

ENV LB_HOW=compile
# Use our download script as entrypoint
ENTRYPOINT ["bash", "-e", "/entry.sh"]
